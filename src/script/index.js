function theme() {
  const light = "is-light";
  const dark = "is-dark";
  const themeKey = "themeMode";

  const body = document.querySelector("body");
  const button = document.querySelector(".c-button");
  const toggleTheme = () => {
    button.addEventListener("click", () => {
      body.classList.toggle(light);
      body.classList.toggle(dark);

      const classes = body.classList.toString();
      preferTheme(classes);
    });
  };
  const preferTheme = (listTheme) => {
    const isLight = listTheme.includes(light);
    return isLight ? savePreferTheme(light) : savePreferTheme(dark);
  };

  const savePreferTheme = (theme) => {
    localStorage.setItem(themeKey, theme);
  };

  (() => {
    const hasTheme = localStorage.getItem(themeKey);
    if (!!hasTheme) {
      console.log(hasTheme);
      body.classList.remove(light, dark);
      body.classList.add(hasTheme);
    } else {
      body.classList.add(dark);
      preferTheme(dark)
    }
  })();

  toggleTheme();
}

theme();
